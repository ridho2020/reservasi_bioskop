<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Movie;
use App\Models\Order;
class OrderController extends Controller
{
    public function index()
    {
        // $orders = Order::with('users', 'movies')->latest()->paginate(10);
        $orders = Order::latest()->paginate(10);

        // dd($orders);
        return view('order.index', compact('orders'));
    }
   public function create()
    {
        $users = User::get();
        $movies = Movie::get();

        return view('order.create', compact('users', 'movies'));
    }

    public function store(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            'user_id' => 'required',
            'movie_id'     => 'required'
        ]);


        $orders = Order::create([
            'user_id' => $request->user_id,
            'movie_id'     => $request->movie_id
        ]);

        if($orders){
            //redirect dengan pesan sukses
            return redirect()->route('order.index')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
            //redirect dengan pesan error
            return redirect()->route('order.index')->with(['error' => 'Data Gagal Disimpan!']);
        }
    }

}
