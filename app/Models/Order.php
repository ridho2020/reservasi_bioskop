<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'movie_id'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_id');
    }
}
